# PMIx
-System management stacks (SMS)
-To achieve exascale performance:
	-application and SMS must orchestrate together
-SMS components may also communicate with each other

# Introduction
-Many Nodes, Many Cores
-Development in hardware
-Development in software as well
	-Programming models to achieve exascale performance
-Models employ a communication library to run these data movements
	-MPI, PGAS, UPC, OpenSHMEM
-Coexisting models need to coordinate with SMS
-PMI needed for exchanging wireup information etc.
-PMI2 made it better but still faces challenges in orchestrating exascale systems
-PMIx will adress issues of PMI

# PMIX Community
-Many people involved
-Any SMS retains right to return "Not supported"

## Motivation
-High launch times
-SMS Components:
	-WLM (Workload manager) scheduling, allocation
	-RM (Resource manager) spawning, monitoring
	-FS (Global file system)
	-FM (Fabric Manager) oversees high speed network
	- RAS (reliability, availability, serviceability) cluster faults, monitoring

-Historically developed independently (excluding proprietary)
-PMIX aims to resolve integration problems by providing a standardized interface.

### Launch support
-Six stages for job launch
	-Job submission with details on resource usage. <- demand for dynamics
	-WLM assigns resources and spawns local instantiations
		-Swarm of requests on FS
	-Process discover local information and publish to local proxy server (rank, endpoint, etc.)
	-Global exchange of information (modex)
	-Execute barrier to ensure all processes are ready

### Memory Efficiency
-Every local proxy has local copy of SMS state
	-Can lead to high usage
-PMIx addresses this by having distributed memory methods

### Programming models
-Many new programming models (OpenMP, ...)
-SMS environments often lack dynamic support -> impl. must develop a complete environment that hides host SMS
-PMIx provides abstracted interaction with SMS

## Standards process
-Not really relevant

## Scope and Architecture
-They place most of the burden on SMS devs
-They take away some of the burden by providing more support for methods that are helpful

## Reference Implementation
-THERE IS A REFERENCE IMPLEMENTATION SOMEWHERE?!?!?!?
-https://github.com/openpmix/openpmix/tree/master/examples

# Current Focus Areas
## Orchestrating the Launch Process
-Revised launch process to exclude global communication
-Eliminate FS problem by pre positioning information
-Four Stages remain:
	-Job submission
		-WLM can get retrieval time for information through PMIx interface (asking FS)
		-WLM can analyze associated libraries through PMIx interface (i.e. ldd)
	-WLM assigns resources to job
		-Can call FS to pre cache information
		-Can call FM to get local requirements?
	-WLM passes init to RM daemons that will spawn local processes
	-Local PMIx Server notifies host RM of connection and things are ready to go

## Event notification
-Communicate faults
-RM can notify application
- Application can notify peers and or SMS of issues
-SMS subsystems can notify RM for any problem

## Flexible resource allocations
-Components:
	-Request additional resources
	-Extend reservation
	-Release allocated resources
	-Lend resources
-Unfortunately not much more is stated

## Job control and monitoring
-Conflict in computation -> significant waste of resources
-Watchdog Heartbeat as one solution
-Monitoring interface added in PMIx2.0
-Just terminate it <- draconian
	-There can be other measurements

## Queries and Logging
-Must be able to query for support

---
Rest is not that relevant anymore.

