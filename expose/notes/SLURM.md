# SLURM

# Overview
Properties:
- Simplicity
- Open Source
- Portability
- Network Adaptability
- Scalability
- Fault Tolerance
- Secure
- Admin friendly

## What is SLURM
- Allocation and access to resources
- Framework for starts, execution, etc.
- srun, scancel, squeue, scontrol are the main commands

## What SLURM is not
- Not intended for complex sites (directly)
	-> Extensions by API
- Does not directly implement Gang scheduling
- Not a general purpose tool

## Architecture
- slurmd runs on every compute node
- slurmctld runs on a management node
- possibility for fail-over
- srun, scancel, sinfo, squeue, scontrol utlities
	- communicate mostly with slurmctld
	- srun also communicates with nodes
- Manages: Nodes, Partitions (set of nodes), jobs, job steps

### Slurmd
- remote shell daemon
- basically acts as a remote agent
- Remote execution
- Stream Copy Service
- Job Control

### Slurmctld
- Has Master or Standby mode (for fail-over)
- Node Manager - ensures nodes are configured properly, ...
- Partition Manager - groups nodes into disjoint sets for management purpose
- Job Manager - Job scheduling, acceptance, ...

### Command Line Utils
- previously mentioned

### Communications Layer
- Berkeley Sockets
- Every node has hostname
- Slurm packs and unpacks in a machine independent format
	- XML was considered but not accepted

### Security
- Any user may cancel own jobs
- Privileged users may modify
	- SLURM config
	- cancel any jobs
	- ...
	- "root" and SlurmUser
- Support authd and munged
- Communication layer security

## Example: Executing a Batch Job
- Already known

# Controller Design
- Multithreaded
- Subsystems:
	- Node, Partition and Job Manager

## Node Management
- Relevant informations:
	- Processor count
	- Memory on node
	- Temporary disk storage
	- State
	- Weight (How likely to be allocated)
	- Feature
	- IP addr
- List of things that each information is used for

## Partition Management
- Data associated to a partition:
	- Name
	- RootOnly flag
	- Associated nodes
	- Partition state
	- Job time limit
	- Max. nodes for a single job
	- Groups permitted to use partition
	- Access shared?
	- Default partition
- Bit maps for node state

## Configuration
- Mostly used by controller

## Job Manager
- Job Parameters:
	- Name, UserID, JobID, Working Directory, Partition, Priority, Constraints, ...
- Functionality:
	- Request resources
	- Reset priority
	- Job status
	- Job signaling
	- Terminate job
	IN THE FUTURE
	- Preempt/resume/checkpoint/restart job
	- Change node count
- Job Step management

## Fault Tolerance
- Description of fail-over mechanism

# Slurmd
- Manages user jobs and does monitoring
- Monitors resource consumption
- accepts job requests from srun and slurmctld

# Command Line Utilities
Skipped

# Job Initiation Design
- Modes:
	- Interactive: stdin/out/err work
	- Batch: Queued, cannot interfere
	- Allocate: User can manually run job steps
- srun -> slurmctld -> srun -> slurmd -> srun

## Basic description of these modes in technical detail (how they work)




