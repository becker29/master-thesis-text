# Einleitung
- Demanding applications may require HPC-Systems.
- Physical simulations
- Mehr Computer -> Mehr Kommunikation
- Gustafson's Law
- Wetter ICON Modell

# Scheduling in HPC
- Bestmögliche Auslastung erwünscht <- finanzielle/ökonomische Aspekte

## Warteschlangen
- FIFO ähnlich ausführen was möglich ist
- Backfilling -> start late jobs earlier if higher prio jobs are not delayed to increase utilization
	- internal fragmentation-like <- difficult to tackle
- conditions need to be fulfilled
	- deadlines can count here is a condition in the future
- Grid computing <- like the energy network

## Plan-based
- Contract between VRM and person submitting work
- Service Level Agreement
- Guarantees need to be upheld

# Problemlage
- limited information available on the state of processes
- not able to make sure which process is started on which node

# MPI
- Threads share memory
- Processes need to send messages
	-> MPI
- Basic necessities
	- grouping of processes
	- identification
	- initialization
	- communicators

## Groups
- Everybody has a rank
- Starts from 0
- continuous ids

## Communicators
- Intra/Inter communication in/between groups
- local group <-> remote group

## Initialization
- World/Session model
	- World model makes COMM_WORLD usable
	- Session model requires manual communicator creation
- MPI_INIT, MPI_INIT_THREAD

## Dynamic processes
- SPAWN and SPAWN_MULTIPLE
- parent and child processes
- child processes have their own WORLD_COMM
- Inter communicator created on spawn


## Processes in MPI
- Uniquely identified through (group, rank)

# OpenMPI
- Allows dynamic processes (Since MPI2)
- implements MPI-version xxx
- Manual start vs. SLURM start.
	- SLURM: possible under/over provisioning
- hostfile
	- Prozesse von oben nach unten hin aufgeteilt
- bind-to will bind the process to a specific core
- alternative: rankfile for individual assigning
- Further details on work specific parts of OMPI source code

# Lösung & Testaufbau
- already known




