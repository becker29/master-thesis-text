# OMPI
- Wide variaty of different MPI implementation
- Basically a new implementation for all

# Introduction
- Heterogeneity in clusters
- Hardware may change on the fly
- MPI implementation must be able to deal with all of it
- Especially network layer transmission errors in large clusters
- The larger, the more processes, higher proability of failure
	- Either Process can deal with failure (checkpointing, etc...) or MPI Impl can deal with it.

=> New MPI implementation required

## Goals of the Open MPI Project
- More than a simple merger of previous projects
	- New software design
	- Concurrent Multi threaded apps
- Handle network errors transparent to apps

# Architecture
- Modular Component Architecture (MCA)
	- manages component frameworks
	- provides services to component frameworks
- Three classes of components:
	- Open MPI components
	- Open Run Time Environment (ORTE)
	- Open Portable Access Layer (OPAL)
- List of many singular components
- Enables research, use of multiple components at the same time

## Module Lifecycle
- Exemplary workflow (BTL):
	- MPI_INIT Framework discovers all components
	- Components queried to check if they want to run
	- Return set of modules that represent distinct network interfaces
	- Query module to determine communication partners
	- MPI_FINALIZE -> Module can clean up

# Implementation details
## OOP
- Basically C instead of C++ for serialization

## Component Discovery and Management
- During configuration components are searched and compiled statically into the MPI lib
- Components can also be built as shared libs
- 3rdparty may only shared shared libs
- Components discovered at runtime
	- Identification via Name and version

## Point to point components
MPI -> PML -> BML -> ...

# Performance
Better than MPICH-GM - more closely to raw baseline

