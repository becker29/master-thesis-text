# VRM

# Introduction
-Resources may include much more than just hardware
-Grid middleware must allocate appropriate resources
-User only needs to outline required resources
-Next Generation Grid (NGG) requires SLA <- European Commission future tech

-Currently only focus on local Resource Management
	-Cannot take job if requirements not fulfilled
	-SLA very limited if only advanced reservations are possible
	
- More malleable applications / diffuse requests must be handled at RMS level as well

# Status Quo and Future Requirements
-Grid systems UNICOREpro, Globu properietary
-Open system desired

## Requirements
-Resource Sharing in virt orgs over the world
-Mandatory: Flexibility, Transparency, Reliability, application of SLAs
- Guarantee QoS

## Implications
-RMSs must be adjusted to become SLA aware
-Features: Advance reservations, diffuse requests, negotiation protocols
-RMS must monitor running applications
-Control cycle: Application, RMS, used resources

## Currently
-co-allocation of multiple resource types only available for Globus but still does not allow SLAs
-Requirements for negotiating SLAs: SNAP

# Architecture of the VRM
Comprised out of Three layers:
-Administrative Domain Controller
-Coupling Module
-Resource Management

Administrative Domain -> pooling of resources that are valid for special set of policies

## ADC
-Hierarchical
-Controls other ADC and is responsible for an AD
-Local admin of an AD has full autonomy
	-Describe which resources are available at what time
	-Not all information must be published
-ADC can join multiple physical resource into a virtual resource

-Steps of negotiation:
	-ADC checks for resources to comply with SLA
	-ADC starts negotiation with matching internal resources
	-Requester may adjust SLA after receiving response
	-Negotation continues iteratively
-If ADC cannot keep SLA at runtime
	-Check for other internal resources
	-Ask upper layer ADC
	-Retrieve required resources within the Grid

## Coupling Modules
-Act as broker between ADC and RMS

-RMS/NMS does support SLA
	-Coupling module must only convert syntax
-RMS/NMS is not SLA-aware but provides information to determine properties like beg/end
	-CM can infer things
-RMS/NMS does not provide anything helpful for SLA
	-Can only process jobs that do not require QoS
- Negotiation occurs the same in any case and the CM reports back to the ADC

## Local Resource Management Systems
-Classical RMS CCS
-NMS for job migration, ...
-NMS should be SLA-aware as well

# Features
-Responsibility at Runtime
-Information Hiding
-Autonomy of Local Admin. Structures
-Providing Virtual Systems (virtual resources)

# Examples
-Simple example of the architecture

## Negotiation Process
-Some basic usecases

## Assured Job Completion
-Checkpointed jobs
	-Checkpoint -> resubmit
	-> Migration of Job to other AD possible
-VRM tries to get time slots
-Cost of migration
-Negotiation priority
	->Cycle stealing, runtime extension or ruling out other jobs for completion
-NMS should be used to guarantee migration of job
-Can also be used to ensure availability

## Fault Tolerance
-SLA cannot be fulfilled -> Must be migrated
-Migration requires CHECKPOINTING!!!
-Otherwise -> restart job on other cluster
-No available resources -> contact next higher AD

