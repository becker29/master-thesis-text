\section{Background}
\label{Background}

Since the early beginnings of computing with computers like the Z1 invented by Konrad Zuse, computation underwent a significant scale increase. Processors became faster every year, with their transistor count doubling approximately every two years, as observed by Gordon E. Moore \cite{roser2024moore}. Nevertheless, even with their growth in transistor count, consequently increasing computational capabilities, a single processor is overwhelmed by demanding applications. 

For example, the accuracy of an n-body simulation generally benefits from decreasing the length of a time step, which increases the amount of time steps to calculate. Accurate simulations can have higher computing requirements than a single processor currently provides. Given a tight time budget, interconnecting multiple computers is the only choice to tackle these computation problems. The type of interconnection can be crucial to the speed of communication and scalability of the application. An example is the weather research and forecast model, which scales well with fast interconnections \cite{shainer2009weather}. Other parallel applications without high communication overhead can even benefit from interconnection via the Internet.

Interconnected computers can form a cluster. The top 500 list \cite{t500} collects the most resourceful computers known in the world. These computers are clusters, each with many individual computers having high-speed interconnections. The developer of programs that run on such devices, even with clusters in the early beginnings, faces multiple challenges.

Firstly, the developer must start the program on as many computers in the cluster as desired. A cluster might be a shared resource between many who can access it. For this reason, programs must make agreements with each other to balance the load between computers. This balancing may not be in the interest of the program's initiator. Because of load balancing, the communication between the individual processes started on the different computers must be handled. The developer usually cannot know beforehand which endpoints must be used for communication when their process placement is unknown.

These requirements gave way to multiple software solutions that tackled these challenges for the developer. A \gls>{rms} can allocate resources for the program's initiator and start it on each reserved computer. Furthermore, the \gls{mpi} is an established standard for interconnecting the programs after they have started to enable communication between them.

Businesses that want to run programs on such clusters will also need to share resources with others determined by the \gls{rms} instead of being able to rely on a \gls{sla} that can guarantee a certain \gls{qos}. We, therefore, emphasize the importance of an \gls{sla}-aware architecture like the \gls{vrm} of Burchard et al. \cite{burchard2004virtual}. We further insist on the relevance of handling dynamic process creation behaviour in such a context to increase control and simplify the assurance of \gls{qos}. Allowing or denying dynamic process creation helps fulfil obligations in a \gls{sla} \cite{linnert2023impact}. For example, we could speed up a program projected to exceed the assured deadline through dynamic process creation.

\subsection{High Performance Computing}

We define performance as the amount of work done per quantity of time. If we were to transfer this definition onto computing, we would measure performance in the amount of computations done per quantity of time. A popular established performance measure is the \gls>{flops} measure. This measure has become popular because floating-point operations took the most time in the 1970s \cite{gustafson1995hint}.

High-performance systems have changed over the years. In 1988, a supercomputer could cost \$3 million and would prove to provide the highest level of performance measured in \gls{flops} when compared to workstations or personal computers \cite[p.~3]{severance2010high}. The gap between supercomputer hardware and workstation or personal computer hardware has shrunk. The authors in \cite{severance2010high} even argue that current personal computers provide high-performance processing. 

Modern supercomputers often comprise powerful individual computers (nodes) interconnected with a high-speed network, which are required to minimize communication times. Gustafson et al. \cite{gustafson1995hint} argue that modern computers are much more limited in data motion than \gls{flops}. The interconnection of computers can influence data motion. Faster interconnections may increase throughput, but low latency is also crucial to fast exchanges of small amounts of data.

We can see why data motion is an essential factor in performance if we consider a practical example. Shainer et al. \cite{shainer2009weather} studied the performance impact of different interconnections in a \gls{hpc} cluster on the weather and research model with the CONUS 12km benchmark. They found that gigabit Ethernet only provided minor performance gains after 20 involved compute nodes. In contrast, when the cluster uses InfiniBand as an interconnection, performance scales almost linearly for the 23 tested compute nodes.

Another point to consider in communication is communication strategies. A communication strategy not utilizing the underlying hardware network topology may result in less performance. Similarly, a system that places two communicating parties far away from one another might increase the communication latency. Therefore, we need to consider software when we talk about \gls{hpc}. 

\subsection{Slurm}

\gls>{slurm}  is a resource management system for clusters. A user that wants to run a program in a \gls{slurm}-managed cluster will have to submit it to \gls{slurm} in the form of a job. In this context, we define a job as a collection of work associated with a user's request. For example, preprocessing input data and postprocessing of results may also be a part of the request. A job step is a set of parallel tasks and is part of a job, like the preprocessing of input data \cite{yoo2003slurm}. A task is an individual process run during a job step.

\subsubsection{Architecture}

\gls{slurm} needs to run multiple different processes to function. Each process has a distinct role in managing the resources in a cluster. 

\begin{figure}[!ht]
	\centering
	\resizebox{0.75\textwidth}{!}{%
		\begin{tikzpicture}
			\tikzstyle{every node}=[font=\large]
			% Interconnections
			\draw  (2,1) rectangle  node {\small slurmd} (0,0);
			\draw  (6,1) rectangle  node {\small slurmd}  (4,0);
			\draw  (0,3) rectangle  node {\small slurmctld} (2,4);
			\draw  (4,3) rectangle  node {\small slurmdbd} (6,4);
			\draw[>=triangle 45, <->] (1,3) -- (1,1);
			\draw[>=triangle 45, <->] (1,3) -- (5,1);
			\draw[>=triangle 45, <->] (2,3.5) -- (4,3.5);
			
			% Roles
			\draw[dashed] (-4, 4.5) rectangle (6.5, 2.5);
			\draw[dashed] (-4, 1.5) rectangle (6.5, -0.5);
			\node[right,align=left] at (-3.5,3.5) {\small Control processes\\ \small of \gls{slurm}};
			\node[right,align=left] at (-3.5,0.5) {\small Compute node\\ \small processes of \gls{slurm}};
			
			\end{tikzpicture}
	}%
	
	\caption{Basic \gls{slurm} architecture.}
	
	\label{fig:basic_arch}
\end{figure}

Figure \ref{fig:basic_arch} visualizes a simple slurm architecture. It encompasses the most relevant permanently active processes and shows the assigned processes' role. The \gls>{slurmctld} and \gls>{slurmdbd} are part of the control processes \cite{slurm_ov}. A \gls>{slurmd} spawns on every compute node of the cluster, and they belong to the compute node processes of \gls{slurm}.

Each process has its responsibilities. The \gls{slurmd} process is responsible for local operations \cite{yoo2003slurm}. It monitors and controls task behaviour on the compute node. Such operations include starting, stopping or accepting tasks. It is also responsible for starting and terminating a \gls>{stepd} for every job step on the compute node. The \gls{stepd} manages I/O, accounting and signalling for their job step.

The \gls{slurmctld} is the most crucial process. It monitors all \gls{slurmd} processes and distributes work to them \cite{yoo2003slurm}. It accepts, allocates resources and manages all jobs. It is possible to have another \gls{slurmctld} as a backup process \cite{slurm_ov}. The \gls{slurmctld} also connects to the \gls{slurmdbd}, which provides a database interface for \gls{slurm}. Additionally, multiple clusters can connect to a single \gls{slurmdbd}.

\subsubsection{Control Mechanisms}

A user who wants to submit a job does not need to directly communicate with the \gls{slurmctld}. Instead, there are multiple command line tools within \gls{slurm} that allow the user to submit, monitor and control their jobs quickly. We will only consider tools relevant to this thesis in the following.

\begin{description}
	\item[sbatch] 
	is a tool which we can use to submit a job script to \gls{slurm}. The job script describes the operations like a shell script but has additional information for \gls{slurm} in the header. The following code snippet is an example of a job script from \cite{slurm_sbatch}.
	
	\begin{BVerbatim}
		#!/bin/sh
		#SBATCH --time=1
		srun hostname |sort
	\end{BVerbatim}
	
	The \lstinline|#SBATCH| directive supplies a parameter to sbatch \cite{slurm_sbatch}. However, we can supply parameters over the command line as well. We run the \lstinline|srun| command in the following line. We go into more detail about the \lstinline|srun| tool later. After the submission and validation, sbatch submits the job to the \gls{slurmctld}. When resources for a job are allocatable, the \gls{slurmctld} can start the job according to a preconfigured scheduling algorithm.
	
	\item[srun] is a crucial tool for job step creation \cite{yoo2003slurm}. It also supports resource allocation, resource constraints and different interaction modes. \lstinline|srun| handles the communication with the \gls{slurmctld} for resource allocation and also initiates the tasks using the \gls{slurmd} on each allocated compute node. In the scope of the thesis, we utilize this tool primarily to allocate resources and run applications. 
	
	\item[squeue] is a utility that users of \gls{slurm} can use to see a list of all jobs and their status. We can see a sample output of the \lstinline|squeue| utility below when we execute it without any parameter.
	
\begin{BVerbatim}
JOBID PARTITION     NAME   USER ST   TIME  NODES NODELIST(REASON)
13    normal    MpiTestS   root PD   0:00      1 (Resources)
9     normal    MpiTestS   root  R   0:31      1 c1
11    normal    MpiTestS   root  R   0:17      1 c2
12    normal    hello      root  R   0:16      2 c[1-2]
\end{BVerbatim}
	
	The \say{\lstinline|JOBID|} column provides the job identifier for each queued and running job \cite{yoo2003slurm}. The \say{\lstinline|NAME|} column shows the name of the submitted batch script or the name of the application. We can distinguish the status of each job with the \say{\lstinline|ST|} column. Currently running jobs have the \say{\lstinline|R|} status, and the \say{\lstinline|TIME|} column shows the total runtime along with the number of nodes and node names in the \say{\lstinline|NODES|} and \say{\lstinline|NODELIST|} column, respectively. Jobs that are pending execution have the \say{\lstinline|PD|} status, and the \say{\lstinline|NODES|} column shows the required number of nodes. The \say{\lstinline|NODELIST|} column provides the reason for the pending execution in this case.
	
	\item[scancel] is a utility that can cancel jobs. We can pass a comma-delimited list of job identifiers to cancel multiple jobs simultaneously.
\end{description}

All presented utilities also have further options that an interested reader can find by passing the \say{\lstinline|--help|} parameter to the respective utility.

\subsubsection{Plugins}
\label{subsec:plugins}

In earlier versions of \gls{slurm}, some plugin types were available where the developer needed to compile the plugin with \gls{slurm} itself \cite{rene}. These plugin types are no longer publicly documented, and instead, the \gls{slurm} RestAPI \cite{slurm_rest} covers some of these functionalities. The plugins could directly access data structures inside of \gls{slurm}. As of the current version of \gls{slurm}, developers can create plugins through the \gls>{spank}, which is a plugin architecture that utilizes a single shared interface. Developers wanting to create a \gls{spank} plugin must include the \say{\lstinline|spank.h|} C header file, compile their library against it and then supply the library to \gls{slurm}. \gls{spank} plugins can only access data that \gls{slurm} provides through the interface. Additionally, the plugin is restricted by only being called at specific points in the job's life cycle.

Crucially, the functionality of some parts of \gls{slurm} still uses an internal plugin-like system. Internal plugins have their dedicated source folder at \lstinline|/slurm/src/plugins|. An example of an internal plugin type is the \gls{mpi} plugin type.

\subsubsection{Job Lifecycle}
\label{subsec:lifecycle}

Jette et al. \cite{yoo2003slurm} show the process a submitted job script goes through, which we briefly outline. Once the user submits a job script with \lstinline|sbatch|, the utility runs \lstinline|srun| on the cluster to enqueue the job. The \gls{slurmctld} can initiate the start of the job after resource allocation and scheduling. The \gls{slurmctld} then notifies the \gls{slurmd} of all allocated compute nodes that they need to start a job step. The \gls{slurmd} then handles the executions of the job step and associated tasks on the compute node in its responsibility.

The \gls{stepd} is of interest for the thesis because it prepares the environment of processes. This environment setup also includes the server-side initialization of PMIx-related information, which is evident because the source file \lstinline|/slurm/src/slurmd/slurmstepd/mgr.c| is where the \gls{mpi} plugin mentioned in section \ref{subsec:plugins} is called. Specifically, the job description is passed in line $1287$. The \gls{mpi} name of the plugin appears to be misleading because most \gls{mpi} plugins in \gls{slurm} implement the server-side of a \gls{pmi} specification. The \gls{stepd} selects the plugin depending on the variant of \gls{pmi} that the user selects.

\subsection{Virtual Resource Manager (VRM)}
\label{subsec:VRM}

The grid is a distributed computing system where the user can select any service it offers without knowing any details, such as location or resource requirements \cite[p.~3]{prabhu2008grid}. Each node in the grid can be an individual or institution that shares its resources. The \gls{ngg} has the goal of resource sharing in virtual organizations over the world \cite{bal2003next} as cited in \cite{burchard2004virtual}.

From a customer's point of view, the usability of a cluster is an essential factor \cite{snelling2002ogsa} as cited in \cite{burchard2004virtual}. For this reason, the \gls>{ngg} requires an assurance of the \gls>{qos}. A possible target businesses can focus on is the completion time, as they may set a deadline for their computing task due to their plans. Such obligations must be negotiated before execution, resulting in a \gls>{sla}. The \gls{sla} outlines the work requirements and is a contract between businesses where the provider assures a certain service level and is liable for not fulfilling obligations.

To fulfil such \glspl{sla}, Burchard et al. \cite{burchard2004virtual} proposed an \gls{sla}-aware management architecture for the grid called \gls>{vrm}. A cluster can be a single node in such a grid. Each node has its resource management. Burchard et al. \cite{burchard2004virtual} notice this and require \glspl{rms} to become \gls{sla}-aware as well.

\begin{figure}[!ht]
	\centering
	\resizebox{0.5\textwidth}{!}{%
		\begin{tikzpicture}
			\tikzstyle{every node}=[font=\large]
			% Interconnections
			\draw  (0,4) rectangle  node {\small Administrative Domain Controller} (6,3);
			\draw  (0,2.5) rectangle  node {\small Active Interface}  (6,1.5);
			\draw  (0,1) rectangle  node {\small Resource Management System} (6,0);
			\draw[>=triangle 45, ->] (3,3) -- (3,2.5);
			\draw[>=triangle 45, ->] (3,1.5) -- (3,1);
		\end{tikzpicture}
	}%
	
	\caption{Outlined layers of the architecture proposed in \cite{burchard2004virtual}.}
	
	\label{fig:vrm_basic_arch}
\end{figure}

Figure \ref{fig:vrm_basic_arch} shows the environment that a \gls{rms} finds itself in the proposed architecture \cite{burchard2004virtual}. The administrative domain controller can be responsible for multiple active interfaces, their representation to the grid and \gls{sla} negotiation. The active interface is a broker between the many possible \gls{rms} and the requirements that the administrative domain controller imposes \cite{burchard2004virtual, linnert2014mapping}.

The active interface plays a vital role in this architecture because it is not a simple wrapper but plays an active role \cite{burchard2004virtual}. \gls{rms} that are not \gls{sla}-aware may provide enough information to infer properties of a job like the completion time. In such a case, the active interface can use this information \cite{burchard2004virtual}. 

\gls{rms} that are fully \gls{sla}-aware only require an active interface to adapt them. \gls{rms} systems that provide no information and are not \gls{sla}-aware only qualify for compute work that does not require any \gls{qos} \cite{burchard2004virtual}. \gls{slurm} is a popular \gls{rms} \cite{SchedMD}, and support for fine grain control and information gathering is actively researched to enable higher \gls{qos} assurance \cite{felix, rene}. 

\subsection{OpenMPI}
\label{subsec:OMPI}

The processes that a job, which we submit to \gls{slurm}, starts will need to communicate with each other to work together. Implementing such communication can be complicated, especially when we desire hardware support in a heterogeneous cluster. The \gls>{mpi} standard abstracts these tasks away from the developer.

OpenMPI is an implementation of the \gls{mpi} standard. Before OpenMPI, many different MPI implementations were specialized in different aspects of \gls{hpc} \cite{OMPI}. Each MPI implementation requires a different configuration, which can prove inefficient for users and administration. Graham et al. \cite{OMPI} introduce OpenMPI as a component-based open-source production-grade MPI implementation. For this reason, it is simpler to extend to other aspects of \gls{hpc} than closed-source alternatives. We will focus on the development with OpenMPI in the following.

When working with an \gls{mpi} implementation, the program must be compiled with additional flags \cite{ompi_compile}. OpenMPI provides the wrapper \lstinline|mpicc| for this purpose. 

\begin{figure}[htb!]
	\begin{lstlisting}[style=CStyle, caption={Exemplary ``hello world'' \gls{mpi} application.},label={lst:helloworldmpi}]
int main(int argc, char** argv) {
	int process_count, process_rank;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &process_count);
	MPI_Comm_rank(MPI_COMM_WORLD, &process_rank);
	printf("Hello world from process  %d of %d\n", process_rank, process_count);
	MPI_Finalize();
}
	\end{lstlisting}
\end{figure}

\lstset{
	literate={\_}{}{0\discretionary{\_}{}{\_}}%
}

An example of an \gls{mpi} application can be seen in listing \ref{lst:helloworldmpi}. The \say{\lstinline|MPI_Init|} method initializes the \gls{mpi} library with the arguments given to the application. We must call this method before we can use the \gls{mpi} library. We also have to call the corresponding \say{\lstinline|MPI_Finalize|} method at the end of the application. Any other \gls{mpi} methods calls are only valid between the initialization and finalization methods. Processes of an \gls{mpi} application can use communicators. The communicator where all initially started processes are is \say{\lstinline|MPI_COMM_WORLD|}. Each process in a communicator has a unique rank that is a number from $0$ to $n$, where $n$ is the number of processes that are part of the communicator group. It is important to note that the rank of the process is vital to its role. Most MPI applications will have a diverse set of work, and the rank can be a method to specify which process does which work.

The methods \say{\lstinline|MPI_Comm_spawn|} and \say{\lstinline|MPI_Comm_spawn_multiple|} can be used to spawn processes dynamically.
\begin{lstlisting}[style=CStyle,label=mpi_spawn,caption={``MPI\_Comm\_spawn'' signature.}]
int MPI_Comm_spawn(const char *command, char *argv[], int maxprocs, MPI_Info info, 
									 int root, MPI_Comm comm, MPI_Comm *intercomm,
									 int array_of_errcodes[])
\end{lstlisting}
Listing \ref{mpi_spawn} shows the signature of the mpi spawn method. The method executes the passed \say{\lstinline|command|} with the given \say{\lstinline|argv|} parameters, and it spawns up to \say{\lstinline|maxprocs|}\\ processes. We can pass further details about the start of the processes with the \say{\lstinline|info|} parameter. The \say{\lstinline|comm|} will be the communicator of the children. \say{\lstinline|intercomm|} and \say{\lstinline|array_of_errcodes|} are returned from the method and can be used to communicate with the spawned processes and to check for any failure \cite{ompi_method_doc}.
If the spawned application is an MPI application, it can access a communicator to the parent via \say{\lstinline|MPI_Comm_get_parent|}. The \say{\lstinline|MPI_Comm_spawn_multiple|} method allows the user to spawn processes with different commands and arguments simultaneously. It is otherwise identical.

\subsection{PMIx}

After a \gls{rms} like \gls{slurm} starts processes that use an \gls{mpi} library, the processes need to undergo a wire-up \cite{castain2017pmix}. This procedure connects processes to enable communication between each other. With the addition of dynamic process spawning, the \gls{rms} needs to be included in the spawning process because it is supposed to manage allocations of resources. Castain et al. \cite{castain2017pmix} refer to the system the processes have to communicate to for such tasks as the \gls{sms}.

OpenMPI uses the \gls{pmi} interface to exchange wire-up information with each participating process. Castain et al. \cite{castain2017pmix} identified that existing \gls{pmi} libraries face scalability and orchestration issues in exascale systems. The wire-up of applications on a big scale takes too long. They claim that PMIx can reduce these issues by removing some restrictions of the \gls{pmi} and extending the standard. Their goal is to reduce the set-up process to less than 30 seconds, for which they have created a baseline test. The PMIx standard is in active development, and the version we use in the scope of this work is the 5.0 version, which is accessible at \cite{PMIxSpec}. The PMIx specification describes methods, data structures and interfaces provided by the PMIx library. \gls{slurm} also supports PMI1 and PMI2, but we will not consider working with these \gls{pmi} implementations due to their issues at exascale as has been mentioned by Castain et al. \cite{castain2017pmix}.

\subsubsection{Architecture}
\label{pmix_arch}

The PMIx standard requires a client-server architecture. The application started on a computer will need to initialize a PMIx client. Libraries that support PMIx can run this initialization process. The application can then use the PMIx client to communicate with the local PMIx server. The PMIx server is integrated into the \gls{sms} and has to be implemented by it \cite{PMIxSpec}. The specification \cite{PMIxSpec} acknowledges that this can place a burden on \gls>{sms} vendors, which is why they assist by taking responsibility for interoperational interfaces between subsystems of a \gls{sms}.

\begin{figure}[htb!]
	\includegraphics[width=\textwidth]{res/pmix_arch.png}
	\caption{PMIx interaction with the \gls{sms}. Taken from \cite{castain2017pmix}}
	\label{fig:pmix_arch}
\end{figure}

Figure \ref{fig:pmix_arch} shows the interactions an application using PMIx and the \gls{sms} can have. The depicted \gls{sms} comprises different subsystems. Most of these subsystems are not relevant to the thesis. However, we can identify that the PMIx server is an integral part of the \gls>{rm}, which in our case is \gls{slurm}. We can see that the PMIx server responds to orchestration requests from PMIx clients. 

This interaction is crucial for the thesis because the PMIx server of \gls{slurm} will need to integrate dynamic process spawning support. The client-side supports dynamic process spawning because OpenMPI supports MPI2 \cite{OMPI}. However, \gls{slurm} does not support dynamic process spawning with PMIx \cite{slurm_mpi}.

We discussed the \lstinline|pmix| plugin in section \ref{subsec:lifecycle}. \gls{slurm} starts the local PMIx server for every single \gls>{stepd} instance which is evident by the \say{\lstinline|pmixp_stepd_init|}\\ method call at \lstinline|/slurm/src/plugins/mpi/pmix/pmixp_server.c| of the \lstinline|pmix| plugin. The \lstinline|pmix| plugin controls the communication with the PMIx client by implementing interfaces that the PMIx specification \cite{PMIxSpec} requires. An important detail of the specification is that PMIx servers may communicate with each other to exchange data across compute nodes. The implementation of the PMIx server is responsible for the communication itself.

\subsubsection{Data Structures}
\label{pmix_structs}

The PMIx library uses multiple data structures recurrently. Each data structure has the prefix \say{\lstinline|pmix\_|} and the suffix \say{\lstinline|\_t|}. We can trace the reason for this naming schema back to the C programming language, which lacks namespaces like in C++. However, the C linker requires unique methods and data structures to identify which declaration the developer references. For this reason, library developers opt to use a prefix to circumvent naming conflicts. Another property of the C programming language is that if a developer wants to use a structure, they need to write \say{\lstinline|struct|} before the structure's name. The PMIx library developer removes this requirement by using a \say{\lstinline|typedef|} that works as an alias for the structure. In the following, we outline basic aliases that the PMIx library uses and that the PMIx specification \cite{PMIxSpec} describes.
\begin{description}
	\item[pmix\_nspace\_t] describes a namespace. The namespace is a string with a maximum length of 255 characters.
	\item[pmix\_rank\_t] describes the rank of a process. The rank is a unique number for each process in a namespace.
	\item[pmix\_key\_t] contains information about a key. The key is a string and has a maximum length of 512 characters.
	\item[pmix\_value\_t] is a data structure that can contain different types of values. It contains type information in the variable \say{\lstinline|type|} and a union variable \say{\lstinline|data|} that contains the data itself.
\end{description}

The PMIx server regularly uses the \say{\lstinline|pmix_proc_t|} and \say{\lstinline|pmix_info_t|} data structures. For this reason, we describe these in further detail.

\begin{lstlisting}[style=CStyle,caption={The ``pmix\_proc\_t'' structure \cite{PMIxSpec}.},label=lstproc]
	typedef struct pmix_proc {
		pmix_nspace_t nspace;
		pmix_rank_t rank;
	} pmix_proc_t;
\end{lstlisting}

Listing \ref{lstproc} shows the \say{\lstinline|pmix_proc_t|} data structure, which identifies a process by providing a namespace and rank. The PMIx specification uses this data structure to describe an individual process. Nevertheless, the rank can also be one of various specific values that indicate a group of processes.

\begin{lstlisting}[style=CStyle,caption={The ``pmix\_info\_t'' structure \cite{PMIxSpec}.},label=lstinfo]
typedef struct pmix_info {
	pmix_key_t key;
	pmix_info_directives_t flags;
	pmix_value_t value;
} pmix_info_t;
\end{lstlisting}

Listing \ref{lstinfo} shows the data structure the PMIx library regularly uses to convey data. The \say{\lstinline|pmix_info_t|} data structure contains a key and value pair and additional flag information. The PMIx library has predefined keys with the \say{pmix.} prefix. Their value types are fixed and predetermined. However, custom key value pairs can also exist.

\subsubsection{Server Methods}
\label{pmix_methods}

The PMIx standard specifies a \say{\lstinline|pmix_server_module_t|} data structure, which can hold all methods a PMIx server could implement. The PMIx server must pass these bundled server methods to the \say{\lstinline|PMIx_server_init|} method \cite{PMIxSpec}. We will inspect the most fundamental methods that a server can provide. Each major PMIx specification version has added new server methods.

It is important to note that the PMIx library passes a callback to most methods. Per the PMIx specification \cite{PMIxSpec}, we must call the callback after the server method has returned. Some methods may bypass this requirement by returning a status that indicates the operation has already been processed. The server methods relevant for the thesis of the PMIx specification \cite{PMIxSpec} are paraphrased in the following.

\begin{description}
	\item[pmix\_server\_client\_connected\_fn\_t (client\_connected)]\hfill\\
	Once a new client connects to the PMIx server, the PMIx library calls the \say{\lstinline|client_connected|} method and passes a \say{\lstinline|pmix_proc_t|} identifying the connected client. As of the latest PMIx specification, the method is deprecated and replaced by the \say{\lstinline|pmix_server_client_connected2_fn_t|} method type, which receives additional \say{\lstinline|pmix_info_t|} values. It is not necessary to call the callback.
	\item[pmix\_server\_client\_finalized\_fn\_t (client\_finalized)]\hfill\\
	The \say{\lstinline|client_finalized|} method is the logical inverse of the \say{\lstinline|client_connected|} method. It only receives a \say{\lstinline|pmix_proc_t|} for process identification. It is not necessary to call the callback.
	\item[pmix\_server\_abort\_fn\_t (abort)]\hfill\\
	The \say{\lstinline|abort|} method is called once a client aborts execution. A passed \say{\lstinline|pmix_proc_t|} identifies the process that initiated the abort. The method also receives the abort message and other processes it must terminate. If the server receives no process identifiers, it must terminate all processes in the same namespace as the initiator. It is not necessary to call the callback.
	\item[pmix\_server \_fencenb\_fn\_t (fence\_nb)]\hfill\\
	The non-blocking \say{\lstinline|fence_nb|} method receives a list of \say{\lstinline|pmix_proc_t|} and a callback. Once every PMIx server responsible for a subset of these processes has its fence method called, every participating PMIx server has to call the callback method. Further \say{\lstinline|pmix_info_t|} values are passed to control additional information collection during the operation. We must call the callback appropriately in this case.
	\item[pmix\_server\_dmodex\_req\_fn\_t (direct\_modex)]\hfill\\
	The \say{\lstinline|direct_modex|} method fetches \gls{bcx} data. This data contains information about a single process that a client has previously set. We must fetch the data from the appropriate PMIx server. The PMIx server that receives the request may have to forward the request to a remote PMIx server. A \say{\lstinline|pmix_proc_t|} is passed to the method to identify the process. The PMIx library can also pass multiple \say{\lstinline|pmix_info_t|} values to determine timeout or required keywords in the \gls{bcx} data. We must call the callback appropriately in this case.
	\item[pmix\_server\_publish\_fn\_t (publish)]\hfill\\
	The \say{\lstinline|publish|} method must store key-value pairs contained in a \say{\lstinline|pmix_info_t|} array and associate them to the \say{\lstinline|pmix_proc_t|} that published them. The PMIx server must differentiate between actual key-value data pairs and instructions passed in the \say{\lstinline|pmix_info_t|} array. The client may control timeout, access policies and persistence. It is not necessary to call the callback.
	\item[pmix\_server\_lookup\_fn\_t (lookup)]\hfill\\
	Other clients can access the key-value pairs that were or will be published using the \say{\lstinline|publish|} method. The \say{\lstinline|lookup|} operation must respond with all key-value pairs available for the requested key-value pairs. A passed \say{\lstinline|pmix_info_t|} array contains instructions, for example, timeout and access policy. It is not necessary to call the callback. However, we must return the results through the callback with the \say{\lstinline|pmix_proc_t|} identifiers that published the key-value pairs.
	\item[pmix\_server\_unpublish\_fn\_t (unpublish)]\hfill\\
	Previously published key-value pairs should be removed by the \say{\lstinline|unpublish|} method. A passed \say{\lstinline|pmix_info_t|} array contains additional instructions. It is not necessary to call the callback.
	\item[pmix\_server\_spawn\_fn\_t (spawn)]\hfill\\
	The \say{\lstinline|spawn|} method is a crucial method for dynamic process spawning. A list of \say{\lstinline|pmix_app_t|} that contains all information to start each application is passed to the method. Notably, each application has a list of \say{\lstinline|pmix_info_t|} that can provide detailed information about the application. A passed \say{\lstinline|pmix_info_t|} array gives further details on how to run the applications. An example of cross-application information is the job timeout, which controls the total maximum runtime of the applications. An example of application information would be the output redirection to a file. It is not necessary to call the callback.
	\item[pmix\_server\_connect\_fn\_t (connect)]\hfill\\
	The \say{\lstinline|connect|} method instructs the server to treat the processes identified by the provided \say{\lstinline|pmix_proc_t|} array as connected. We must treat all connected applications the same way on the failure of a single process. Let the default behaviour be the termination of the application on failure; then, we must terminate all connected processes when a single application fails. It is not necessary to call the callback. However, this method acts as a collective operation because we need to call the callback after the \say{\lstinline|connect|} method has been called for every local PMIx server responsible for a process in the list.
	\item[pmix\_server\_disconnect\_fn\_t (disconnect)]\hfill\\
	The \say{\lstinline|disconnect|} method is the logical inverse of the \say{\lstinline|connect|} method. The callback has the same requirements.
\end{description}