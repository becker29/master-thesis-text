\section{Introduction}
\label{Introduction}

Performance measures how much work can be done in a timespan. \gls>{hpc} concerns itself with maximizing the performance of a computer or a collection of connected computers. An \gls{hpc} system comprises computers or components that enable \gls{hpc} due to their cooperation. It should be noted that a single exceptionally performant computer can also be a \gls{hpc} system.

Managing a collection of computers can become complex. Examples of introduced complexity are heterogeneous components and workload management to ensure the system's resources are well utilized. A system that is capable of managing \gls{hpc} systems is \gls>{slurm}. SchedMD is the company behind \gls{slurm}, and according to them, a wide variety of \gls{hpc} systems use \gls{slurm} \cite{SchedMD}. Companies worldwide rely on the workload management of \gls{slurm} for their \gls{hpc} systems.

Businesses often utilize deadlines in their plans. An example of this is the delivery of supplies in logistics, which is often associated with a fixed date. A failure of the logistics service to uphold the agreed deadline can result in lost revenue for the customer \cite{kerkhoven2023monitoring}. Similarly, a company could rely on simulation results in their plans for a product iteration. If these results are not present when required, employees may be unable to improve the product because they miss the knowledge gained from the simulation. Even worse, the launch of the final product may have to be postponed, leading to potential revenue loss. For this reason, \glspl>{sla} are vital. \glspl>{sla} are contracts that contain agreed obligations in the form of \gls>{qos} and liabilities between the service provider and the customer.

In the context of \gls{slurm}, we can call work items, like a simulation, a job. \gls{slurm} can use one of two scheduling algorithms to decide which jobs are run at which time. The builtin scheduling will first run jobs with higher priority \cite{SchedMD}. The backfill scheduler will also prefer jobs with higher priority but will run small jobs first if large jobs cannot start because of lacking resources \cite{SchedMD}. Both scheduling algorithms are not suitable for companies that require \glspl{sla} because  \gls{slurm} cannot guarantee \gls{qos} for deadlines with them.

Burchard et al. \cite{burchard2004virtual} proposed an architecture for resource management aware of \glspl{sla}. This architecture can enable plan-based scheduling on \gls{hpc} systems to ensure completion times. Ongoing research tries to integrate \gls{slurm} into the \gls{sla}-aware architecture. These integration efforts are due because \gls{slurm} is one of the most popular resource management utilities \cite{SchedMD,t500}. Buchard et al. \cite{burchard2004virtual} call the \gls{sla}-aware architecture the \glsfirst>{vrm}. The \gls{vrm} is a management system that has access to other \glspl>{rms} like \gls{slurm}. Through this access, it is capable of providing \glspl{sla} by selecting computation sites, like a \gls{hpc} system, that have a \gls{rms} capable of assuring a certain level of \gls{qos}.
% Warum muss überhaupt was verwaltet werden <- kein Plan?

\subsection{Motivation}

\gls{hpc} systems can contain various hardware. Software that runs inside a \gls{hpc} system must be able to use the hardware optimally to perform well. Applications prefer to parallelize their workload when possible. The underlying \gls{rms} must know which applications use how many resources to reserve appropriate resources or allocate resources to a different application. If one computer in a \gls{hpc} system is responsible for computations, then we call this computer a compute node. Applications can also run across multiple compute nodes. The programmer of such an application often cannot know beforehand which compute node it runs on. For this reason, applications also require wire-up support to communicate with each other \cite{castain2017pmix}.

The libraries that implement the \gls>{mpi} allow processes of an application to communicate with each other. A \gls{mpi} library is also responsible for the communication between individual processes over different network types and partially for network fault handling \cite{OMPI}. To communicate with the \gls{sms}, \gls{mpi} libraries may make use of a \gls{pmi} library such as PMIx \cite{castain2017pmix}. \gls{mpi} uses this communication to the \gls{sms} to wire up the communication between processes and report events like an application's failure. \gls{slurm} supports multiple different \gls{mpi} and \gls{pmi} libraries, but their support for different features of the Interfaces varies \cite{slurm_mpi}.

The PMIx library implements the \gls>{pmi}, which aims to be suitable for exascale and to eliminate orchestration issues in PMI2 \cite{castain2017pmix}. The Frontier system has already achieved exascale as of November 2023 \cite{t500}, which shows the relevance of PMIx for future \gls{hpc} systems. The associated PMIx standard is actively worked on as the latest PMIx 5.0 Specification release shows \cite{PMIxSpec}. OpenMPI is an actively worked on, open-source \gls{mpi} implementation adaptable by its modular design \cite{OMPI}. OpenMPI supports the MPI2 specification. For this reason, a user can spawn dynamic processes within an \gls{mpi} process \cite{gropp1998mpi}.

The preliminary results that Linnert et al. \cite{linnert2023impact} have presented show that dynamic resource usage is vital for executing more jobs as agreed in \glspl{sla}. Simulations often need to deal with dynamic data. For example, the simulation of airflow through a turbine heavily depends on the shape of each participating part. Such applications can benefit from dynamically creating new processes that handle emergent subproblems in the simulation. Within traditional static allocation, this calls for internal workload management of tasks within the application. However, internal workload management can cause suboptimal cluster utilization when the dynamic behaviour of the application does not demand many resources. Dynamic process spawning is essential in assuring \gls{qos} in \gls{sla}-aware systems because a management system can allow or deny additional processes if the current execution time threatens the assured obligations of an \gls{sla}.

\subsection{Problem Statement}

We prefer the PMIx library as \gls{pmi} implementation because of its relevance in future systems due to its exascale capabilities. We also prefer OpenMPI as a \gls{mpi} implementation because it is open-source and in active development. However, \gls{slurm} does not support the MPI2 specification regarding dynamic process spawning for PMIx combined with OpenMPI \cite{slurm_mpi}. Due to the benefits as mentioned earlier of \glspl{sla} for businesses, it is also relevant to embed a potential dynamic process spawning support into the \gls{sla}-aware architecture of Burchard et al. \cite{burchard2004virtual}.

For these reasons, we require the extended PMIx support in \gls{slurm} for dynamic process spawning using OpenMPI in the plan-based environment of the proposed \gls{sla}-aware architecture.

\subsection{Previous Work}

The \gls{vrm} requires control, information and modification capabilities for \gls{slurm} to enforce plan-based scheduling. \cite{felix} and \cite{rene} target these aspects. Both works are part of an ongoing effort to enable \gls{slurm} to be suitable in a plan-based environment. It is important to state that the \gls{vrm} does not exist as software. Instead, \cite{rene} uses a mocked-up \gls{vrm} for communication.

A previous software project combined the works of \cite{felix} and \cite{rene} to prove that a mock \gls{vrm} could influence the \gls{mpi} rank of individual processes. The work uses a modified version of OpenMPI that can adjust the process ranks. Their test case uses PMIx and OpenMPI to run in a virtual \gls{slurm} cluster. The established architecture, communication capabilities with the mock \gls{vrm} and set-up development environment can be utilized for this thesis.

\subsection{Contribution}

The thesis contributes to the PMIx support of \gls{slurm} for dynamic process spawning and their handling in the plan-based context of the \gls{vrm} project. Concretely, we

\begin{itemize}
	\item provide a proof of concept implementation of dynamic process spawning for \gls{slurm} with PMIx and OpenMPI.
	\item enable the mock \gls{vrm} to be capable of processing dynamic process spawn requests.
	\item outline the limitations of the proof of concept.
	\item present possible steps for future work to refine the proof of concept.
\end{itemize}

\subsection{Organization}

We divide the thesis into three segments. We first introduce fundamental knowledge in the section \ref{Background} and \ref{Current State}. Section \ref{Background} contains information on the environment of the thesis. We lay out the required knowledge about \gls{hpc}, \gls{slurm}, \gls{vrm}, OpenMPI and PMIx as well as their interactions. Section \ref{Current State} concerns itself with the current state of previous work on which we base the thesis. We survey existing components, their purpose, and the existing architecture.
After introducing fundamental knowledge, we describe our contributed software's development process and technical details in sections \ref{Methodology} and \ref{Implementation}. In section \ref{Methodology}, we detail the requirements set for the software and the development methods we use for development. We also introduce test cases we use to validate the requirements. In section \ref{Implementation}, we describe the implemented software and how it is embedded into the previous work.
Finally, we show results and conclude the thesis with sections \ref{res}, \ref{future} and \ref{Conclusion}. Section \ref{res} presents test case results and details any unexpected behaviour. We provide future work that we expect a working prototype to require in section \ref{future}. At last, we conclude the thesis and reflect on the methodology and taken approaches in section \ref{Conclusion}.












