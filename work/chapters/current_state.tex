\section{Current State}
\label{Current State}

The work in \cite{rene} concerned itself with the extension of \gls{slurm} for advanced reservations. An incoming job could provide an execution plan, which a mocked version of the \gls{vrm}, described in section \ref{subsec:VRM}, validates. Additionally, the mock \gls{vrm} could control the job start and state change through a scheduler plugin, which delegated the scheduling to the mock \gls{vrm}. The work in \cite{rene} also contributed to establishing a development and testing environment.

The work in \cite{felix} contributes to control process placement better by modifying OpenMPI to allow for rank swapping. The rank can play a vital role in determining what a process does in the scope of the application, as has been mentioned in subsection \ref{subsec:OMPI}. 

A software project combined both enhancements. It aimed to establish the possibility for the OpenMPI modification to communicate with the mock \gls{vrm} in the environment created in \cite{rene} in addition to the advanced reservation capabilities. The software project also discovered that a Rest API provides many control mechanisms in a more recent \gls{slurm} version, which made the scheduler plugin of \cite{rene} redundant. However, the thesis is also interested in the established development environment and the efforts to increase \gls{qos} in \gls{slurm}.

\subsection{Development Environment}

No testing cluster was available for the work in \cite{rene} and the software project. The implementation was tested with a virtual cluster created through docker \cite{rene}. The virtual cluster runs on a single machine, which can influence the testing capabilities. For example, we can only simulate a cluster appropriate for the system's resources, making it infeasible to gather sufficient data to judge the stability of a solution in a practical setting. We cannot guarantee a solution to work on different hardware because the virtual cluster always uses the same emulated systems.

\begin{wrapfigure}{l}{0.42\textwidth}
	\centering
	\resizebox{0.42\textwidth}{!}{%
		\begin{tikzpicture}
			% Interconnections
			\draw  (0,4) rectangle  node {\small Download Packages} (6,3);
			\draw  (0,2.5) rectangle  node {\small Compile \gls{slurm} and OpenMPI}  (6,1.5);
			\draw  (0,1) rectangle  node {\small Install \gls{slurm} and OpenMPI} (6,0);
			\draw (0,-0.5) rectangle node {\small Compile and install the extension} (6, -1.5);
			\draw[>=triangle 45, ->] (3,3) -- (3,2.5);
			\draw[>=triangle 45, ->] (3,1.5) -- (3,1);
			\draw[>=triangle 45, ->] (3,0) -- (3,-0.5);
		\end{tikzpicture}
	}%
	
	\caption{Workflow for the creation of the docker image in the software project.}
	
	\label{fig:docker_process}
\end{wrapfigure}

Figure \ref{fig:docker_process} shows the sequence of operations in the dockerfile. The docker-compose utility can use this dockerfile to create multiple containers representing each computer in a cluster. It is important to note that docker keeps track of changes and only reruns dependent steps. For example, if the packages change, the whole image must be rebuilt. However, if only the extension changes, it is the only step to be rerun \cite{docker_optimize}.

The docker-compose utility uses a \lstinline|docker-compose.yml| file to create and configure multiple containers from images pulled from the internet or created locally. The virtual cluster comprises the compute nodes, control node, database, rest API service and the mock \gls{vrm}.

% Start of text after wrapfigure

The virtual cluster has two compute nodes named  \say{\lstinline|c1|} and \say{\lstinline|c2|}. The control node container has the name \say{\lstinline|slurmctld|} concerning the \gls{slurmctld} that runs on it. The database is a MySQL database, and the \say{\lstinline|slurmdbd|} container runs the respective \gls{slurmdbd} that communicates with it. \gls{slurm} requires the database to store accounting records persistently \cite{slurm_ov}. The containers \say{\lstinline|slurmrestd|} and \say{\lstinline|vrm|} are responsible for the Rest API service and the mock \gls{vrm}, respectively.

\subsection{Extension}
\label{ext}

The work of the software project was focused on the combination of the plan-based \gls{slurm} prototype in \cite{rene} and the OpenMPI rank shuffling in \cite{felix}. The software project's goal was to include the OpenMPI rank shuffling in the virtual docker cluster and implement a system that enables all of the required communication. We will call the resulting system the \say{extension} because it has a strictly separate source code located at \lstinline|/extension|.

\subsubsection{Architecture}
\label{ext_arch}

The software project's implementation comprises a control agent and Dynamic Process Management agents (DPM agents). The DPM agent is a \gls{spank} plugin for \gls{slurm} that runs in the context of the \gls{slurmd} as a separate thread. It communicates with the OpenMPI modification in \cite{felix} and the control agent. The control agent is a stand-alone application that runs on the \say{\lstinline|slurmctld|} container. It manages the communication with the DPM agents and the mock \gls{vrm}.

\begin{figure}[!ht]
	\centering
	\resizebox{0.7\textwidth}{!}{%
		\begin{tikzpicture}
			\tikzstyle{every node}=[font=\large]
			% Interconnections
			\draw  (6,1.2) rectangle (0,-0.2);
			\node[align=center] at (1,0.5) {\small \gls{slurmd}};
			\draw  (5.8,1) rectangle  node {\small DPM Agent} (3,0);
			
			\draw  (0,3) rectangle  node {\small slurmctld} (2,4);
			\draw  (5.8,4) rectangle  node {\small Control Agent} (3,3);
			\draw  (5.8,7) rectangle  node {\small Mock \gls{vrm}} (3,6);
			\draw[>=triangle 45, <->] (1,3) -- (1,1.2);
			\draw[>=triangle 45, <->] (4.4,3) -- (4.4,1);
			\draw[>=triangle 45, <->] (4.4,4) -- (4.4,6);
			
			% Containers
			\draw[dashed] (-4, 7.5) rectangle (6.5, 5.5);
			\draw[dashed] (-4, 4.5) rectangle (6.5, 2.5);
			\draw[dashed] (-4, 1.5) rectangle (6.5, -0.5);
			\node[right,align=left] at (-3.5,6.5) {\small Container\\ \small \say{vrm}};
			\node[right,align=left] at (-3.5,3.5) {\small Container\\ \small \say{slurmctld}};
			\node[right,align=left] at (-3.5,0.5) {\small Containers\\ \small \say{c1} and \say{c2}};
			
		\end{tikzpicture}
	}%
	
	\caption{Architecture of the extension with \gls{slurm} in the virtual cluster.}
	
	\label{fig:current_state_arch}
\end{figure}

Figure \ref{fig:current_state_arch} visualizes these interactions. We can see that the additional branch that consists of the mock \gls{vrm}, control agent and DPM agent have a similar hierarchy to the \gls{slurm} architecture. The only exception is the DPM agent, which integrates into the \gls{slurmd}.

\begin{figure}[!htb]
	\centering
	\resizebox{\textwidth}{!}{%
		\tikzumlset{fill component=white!20}
		\begin{tikzpicture}
			\begin{umlcomponent}{Common Module}
				\umlbasiccomponent{Config}
				\umlbasiccomponent[y=-3,x=2.5]{Event System}
				\umlbasiccomponent[x=10,y=-3]{Networking}
				\begin{umlcomponent}{Messaging}
					\umlbasiccomponent[x=6]{Messages}
				\end{umlcomponent}
			\end{umlcomponent}
			\umlHVassemblyconnector[interface=ES Interface, arm2=2cm, last arm]{Messaging}{Event System}
			\umlHVassemblyconnector[interface=N Interface, arm1=2cm, last arm]{Messaging}{Networking}
			
			\umlbasiccomponent[x=-0.5,y=-7.5]{DPM Agent}
			\umlbasiccomponent[x=3.35,y=-7.5]{Control Agent}
			\umlbasiccomponent[x=7.2,y=-7.5]{Mock VRM}
			\umlbasiccomponent[x=11,y=-7.5]{Job Validator}
			\umlVHVassemblyconnector[interface=Common Interface, arm2=-1.5cm, last arm]{DPM Agent}{Common Module}
			\umlVHVassemblyconnector[arm2=-1.5cm, last arm]{Control Agent}{Common Module}
			\umlVHVassemblyconnector[arm2=-1.5cm, last arm]{Mock VRM}{Common Module}
			\umlVHVassemblyconnector[arm2=-1.5cm, last arm]{Job Validator}{Common Module}
		\end{tikzpicture}
	}%
	
	\caption{Software module architecture of the extension.}
	
	\label{fig:current_state_code_modules}
\end{figure}

All software artefacts of the extension share software modules, and code specific to a single application or plugin is kept minimal. Figure \ref{fig:current_state_code_modules} shows the modularized architecture of the extension. Every application and plugin of the extension uses the methods that the \say{\lstinline|common|} module provides. The \say{\lstinline|common|} module contains the \say{\lstinline|config|}, \say{\lstinline|event system|}, \say{\lstinline|messaging|} and \say{\lstinline|networking|} modules. The \lstinline|/extension/src/common/| folder contains all these submodules. Crucially, the messaging module also contains all messages that extension applications can send to each other. This locality guarantees that any extension application interprets and serializes the message identically.

\FloatBarrier

\subsubsection{Job Validator}
\label{Job Validator}

After the software project, the job validator in \cite{rene} has remained as a \gls>{spank} plugin. For this reason, \gls{slurm} utilities call it at specific points during their execution. Figure \ref{fig:validator_flow} shows the validation process.

\begin{figure}[!htb]
	\centering
	\resizebox{\textwidth}{!}{
		\begin{sequencediagram}
			\renewcommand\unitfactor{0.4}
			\newthread{U}{\scriptsize User}{}
			\newinst{B}{\scriptsize sbatch}{}
			\newinst{S}{\scriptsize Slurm}{}
			\newinst{J}{\scriptsize Job Validator}
			\newinst{M}{\scriptsize Mock VRM}
			\begin{call}{U}{\tiny Submit job script}{B}{\tiny Job submitted}[padding=-2.5]
				\begin{call}{B}{\tiny Call SPANK plugin}{J}{\tiny Job valid}
					\begin{call}{J}{\tiny Validate plan}{M}{\tiny vrm\_job\_id}
					\end{call}
					\begin{messcall}{J}{\tiny Set environment variable}{B}
					\end{messcall}
				\end{call}
				\begin{messcall}{B}{\tiny Schedule job}{S}
				\end{messcall}
			\end{call}
		\end{sequencediagram}
	}
	
	\caption{Sequence diagram of the job validation process.}
	
	\label{fig:validator_flow}
\end{figure}

On submission of a job script with the \lstinline|sbatch| utility, it consults the job validator about the job's validity. The job validator transmits the request to the mock \gls{vrm}. If the job is invalid, the \gls{vrm} could respond appropriately. In this case, the validator will deny the submission. If the job is valid, then the \gls{vrm} must respond with an internal identifier that further communication about the job should use. In the following, we will call this identifier the \say{\lstinline|vrm_job_id|}.

The job validator appends the \say{\lstinline|vrm_job_id|} to the environment of the job. The environment variable used for this purpose is \say{\lstinline|SLURM_VRM_JOBID|}. Afterwards, the validator returns that the job is valid. The plugin then allows job scheduling by returning the validity to the \lstinline|sbatch|.

\subsubsection{DPM Agent}

The OpenMPI modification in \cite{felix} has to communicate with a local agent. More precisely, each spawned \gls{mpi} process communicates information about itself to the local agent \cite{felix}. The DPM agent is the concrete implementation of the mocked local agent in \cite{felix}. It forwards all local \gls{mpi} process information to the control agent, which returns a rank shuffle that the mock \gls{vrm} has determined. The DPM agent is then responsible for assigning the new ranks to each \gls{mpi} process.

The DPM agent is a \gls>{spank} plugin and starts a thread when the \gls{slurmd} initializes it with the \say{\lstinline|slurm_spank_init|} method. The thread remains alive until the \gls{slurmd} encounters an unrecoverable error or terminates and calls the \say{\lstinline|slurm_spank_slurmd_exit|} method, which terminates the thread as we can see in \lstinline|/extension/src/dpm-agent/main.cpp|.

Every DPM agent registers with the control agent. The lifetime of the established session corresponds to the DPM agent's lifetime. This connection allows the control agent to identify the source node of any request.

\begin{figure}[!htb]
	\centering
	\resizebox{\textwidth}{!}{
		\begin{sequencediagram}
			\renewcommand\unitfactor{0.4}
			\newthread{SS}{\scriptsize Slurm}
			\newinst{P}{\scriptsize MPI applications}
			\newthread{D}{\scriptsize DPM agent}
			\newthread{C}{\scriptsize Control agent}{}
			\newthread{S}{\scriptsize \gls{slurmd}}
			
			\begin{messcall}{S}{\tiny Call SPANK plugin}{D}
			\end{messcall}
			\prelevel
			\begin{messcall}{D}{\tiny Register}{C}
			\end{messcall}
			\prelevel
			\begin{call}{SS}{\tiny Start applications}{P}{}
				\postlevel
				\begin{messcall}{P}{\tiny Process info 1}{D}
				\end{messcall}
				\prelevel
				\begin{messcall}{P}{\tiny Process info 2}{D}
				\end{messcall}
				
				\prelevel\prelevel\prelevel\prelevel\prelevel
				\begin{sdblock}{}{}
					\begin{messcall}{D}{\tiny Process info 1}{C}
					\end{messcall}
					\prelevel
					\begin{messcall}{D}{\tiny Process info 2}{C}
					\end{messcall}
					
					\begin{messcall}{C}{\tiny Rank shuffle}{D}
					\end{messcall}
				\end{sdblock}
				\prelevel\prelevel\prelevel
				\begin{messcall}{D}{\tiny Rank shuffle}{P}
				\end{messcall}
				\prelevel
				\begin{call}{P}{\tiny Run}{P}{}
				\end{call}
			\end{call}
		\end{sequencediagram}
	}
	
	\caption{Sequence diagram of the rank shuffling process from the perspective of the DPM agent.}
	
	\label{fig:dpm_agent_flow}
\end{figure}

Figure \ref{fig:dpm_agent_flow} visualizes this process for two MPI processes. The DPM agent only acts as a process and rank shuffle information forwarder. The DPM agent also blocks the \gls{mpi} application until it receives the rank shuffle, which it does by caching the connection and sending the response when it is available. Because of the caching, the DPM agent can handle multiple requests simultaneously. Section \ref{subsec:control_agent} further details the box in the diagram.

\subsubsection{Control Agent}
\label{subsec:control_agent}

The \gls{vrm} may only enact a rank shuffle once every \gls{mpi} process has been spawned and waits for a rank modification. To accomplish this requirement, the control agent does a global collective of all process information sent by DPM agents and presents the summarized information to the \gls{vrm}. The \gls{vrm} can then respond with a permutation of the ranks, which the control agent forwards to each node participating in the job.

\begin{figure}[htb!]
	\centering
	\resizebox{\textwidth}{!}{
		\begin{sequencediagram}
			\renewcommand\unitfactor{0.4}
			\newthread{D1}{\scriptsize DPM agent c1}
			\newthread{D2}{\scriptsize DPM agent c2}
			\newthread{C}{\scriptsize Control agent}
			\newthread{M}{\scriptsize Mock VRM}
			
			\begin{messcall}{D1}{\tiny Process info 1}{C}
			\end{messcall}
			\prelevel
			\begin{messcall}{D2}{\tiny Process info 2}{C}
			\end{messcall}
			
			\prelevel
			\begin{messcall}{C}{\tiny Job spawn info}{M}
			\end{messcall}
			
			\prelevel
			\begin{call}{M}{\tiny Determine rank shuffle}{M}{}
			\end{call}
			
			\begin{messcall}{M}{\tiny Rank shuffle}{C}
			\end{messcall}
			
			\prelevel
			\begin{messcall}{C}{\tiny Rank shuffle}{D2}
			\end{messcall}
			\prelevel
			\begin{messcall}{C}{\tiny Rank shuffle}{D1}
			\end{messcall}
			\prelevel
		\end{sequencediagram}
	}
	
	\caption{Sequence diagram of the rank shuffling process from the perspective of the control agent.}
	
	\label{fig:control_agent_flow}
\end{figure}

Figure \ref{fig:control_agent_flow} describes the rank shuffling process for two \gls{mpi} processes from the perspective of the control agent. The processes can be on entirely different nodes, and the order of their process information does not need to be in sequence.

The control agent has two caches that help in the rank shuffling process. It has a node cache that collects registered DPM agents and associates them with their node names. The job cache constructs complete job information from the process information. It associates each process to the node name that it runs on. As per the architecture of \gls{slurm}, every compute node runs a \gls{slurmd}, which runs a DPM agent. The control agent can, therefore, use both caches to collect and distribute the necessary data. Similarly to the DPM agent, the control agent does not block at any time. Therefore, it is capable of handling multiple requests simultaneously.

\FloatBarrier

\subsubsection{Mock \gls{vrm}}

The extension requires a mock \gls>{vrm} because the \gls{vrm} does not exist as software. The mock \gls{vrm} implements the job validation and rank shuffling for the extension. Figures \ref{fig:validator_flow} and \ref{fig:control_agent_flow} show all possible communication with the mock \gls{vrm}. In case of a job validation request, it always responds that the plan is valid and provides a unique \say{\lstinline|vrm_job_id|}. We can also deny the job validation request if we adjust the behaviour of the mock \gls{vrm}.

The rank shuffling occurs randomly with the help of the \say{\lstinline|std::shuffle|} method, which can shuffle an array of values. The shuffled array determines the new ranks of processes in the following way: Let $i$ be the rank before shuffling and $a$ be the shuffled array, then $a[i]$ determines the new rank.

\subsubsection{Communication}
\label{subsec:communication}

The \say{\lstinline|messaging|} module determines how the extension's applications and plugins communicate. The DPM agent, control agent and mock \gls{vrm} each run a TCP server. They can initiate communication by contacting the appropriate hosts, which the configuration file determines. The \say{\lstinline|config|} module interprets the configuration and provides the configured values to the application. The \say{\lstinline|messaging|} module uses a context-aware JSON format for data transfer. Concretely, the message is always in JSON format, but the payload can be a JSON or a string.

\begin{lstlisting}[language=json, caption={Process information message from the OpenMPI modification.},label={lst:procinfoompi},captionpos=b]
	{"msg_type": 128, "msg_data": "75,0,1,1,1"}
\end{lstlisting}

The data sent in the OpenMPI modification context is compressed, as seen in listing \ref{lst:procinfoompi}, which shows the process information message from an \gls{mpi} process. In this compressed format, the \say{\lstinline|msg_data|} key maps to the compressed data as a string. Every message also contains the \say{\lstinline|msg_type|} key for identifying the message type.

For internal communication between applications and plugins with access to the \say{\lstinline|messaging|} module, the \say{\lstinline|msg_data|} key maps to a JSON payload. The reason for a compressed format is the increased complexity of the JSON format for C applications. This circumstance is relevant for the thesis because \gls{slurm} as well as the internal \lstinline|pmix| plugin mentioned in section \ref{subsec:lifecycle} and \ref{pmix_arch} is also written in C. Messages that the extension can send to C applications have a toggle allowing string serialization.

\begin{lstlisting}[language=json, caption={Forwarded process information by the DPM agent},label={lst:procinfohr},captionpos=b]
	{"msg_data":{
			"job_id":1,
			"pid":75,
			"process_count":1,
			"rank":0,
			"vrm_job_id":"1"
		},
		"msg_type":128
	}
\end{lstlisting}

Listing \ref{lst:procinfohr} shows the uncompressed human-readable form of the same message as in listing \ref{lst:procinfoompi}. Here, the \say{\lstinline|job_id|} refers to the job identifier assigned by \gls{slurm}. The \say{\lstinline|pid|} refers to the process identifier assigned to the process by Linux. \say{\lstinline|rank|}\\ and \say{\lstinline|process_count|} are \gls{mpi} information as has been described in section \ref{subsec:OMPI}. Finally, the \say{\lstinline|vrm_job_id|} is the internal identifier of the job in the \gls{vrm}, which the job validator previously wrote to the \say{\lstinline|SLURM_VRM_JOBID|} environment variable as section \ref{Job Validator} describes.

Listing \ref{lst:vrmjobvalid} shows the job validation mechanism inside of the mock \gls{vrm}, which we can use to understand how message handling and distribution work inside an application of the extension.

\begin{lstlisting}[style=CppStyle,caption={Job validation logic in the mock \gls{vrm}},label={lst:vrmjobvalid}]
	void handlePlanValidation(std::shared_ptr<messaging::MessageEvent> event, const messaging::Context &context) {
		using Response = messaging::message::JobPlanResponse;
		auto msg = std::dynamic_pointer_cast<messaging::message::JobPlan>(event);
		
		if (msg->GetPlan().size() > 0) {
			g_MessageDistributor->SendMessage(context, 
				Response(Response::Code::STATUS_VALID, g_JobId++));
			return;
		}
		g_MessageDistributor->SendMessage(context, 
			Response(Response::Code::STATUS_INVALID, 0));
	}
\end{lstlisting}

Each received message requires a handler to process it. In the case of listing \ref{lst:vrmjobvalid} it is \say{\lstinline|handlePlanValidation|}. Each handler must be associated with a context through a dispatcher in order to receive message events for a given client session. The dispatcher receives messages from the associated sessions and calls the appropriate subscribed handlers with the message event and context. The context describes the environment in which the message event occurred.

The handler then casts the event to the message type it should handle and continues with the program logic. It sends a response through a distributor with the context of the message event. Crucially, the context also stores the network session itself, which will only stay active as long as the context is stored.

\subsection{\gls{slurm}}
\label{state_slurm}

The extension's source code is outside the \gls{slurm} source code. The scheduler plugin of \cite{rene} was incompatible with a newer version of \gls{slurm}, and the Rest API replaced the functionality.

As section \ref{pmix_arch} mentions, the \gls{rm} is responsible for the PMIx server. \gls{slurm} has a plugin for the PMIx support located at \lstinline|/slurm/src/plugins/mpi/pmix|. The developers of \gls{slurm} have already created a barebone structure for PMIx support of dynamic process spawning located at \lstinline|/slurm/src/plugins/pmix/pmixp_client_v2.c|, which provides all methods of the \say{\lstinline|pmix_server_module_t|}. However, all methods miss an implementation, which results in a non-functioning dynamic process spawning. Every method returns an error code that signals this implementation state to the PMIx clients.

\subsection{OpenMPI}

The modification described in \cite{felix} is located in \lstinline|/ompi/communicator/comm_init.c|.\\ When OpenMPI initializes a communicator, it runs the \say{\lstinline|get_modified_ranks|} method of \cite{felix}. The method looks for the environment variable \say{\lstinline|SLURM_VRM_JOBID|} that has been set by the job validator. It will then contact the DPM Agent with information about the current process. Afterwards, the process is blocked until the DPM agent responds with the modified ranks.

We cannot narrow the PMIx support of OpenMPI down to a single source folder or file. OpenMPI uses PMIx library methods widely. We can observe that OpenMPI will terminate the testing application from the software project as soon as we call process spawning methods. The displayed error can be associated with the error codes mentioned in section \ref{state_slurm} for barebone PMIx support for dynamic process spawning.