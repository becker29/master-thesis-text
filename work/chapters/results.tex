\section{Results}
\label{res}

We evaluate the success of the implementation through the evaluation of the requirements. We use the three test cases mentioned in section \ref{req} to verify the implementation's capabilities. Each test case uses a different mode of the test application. We introduce additional parameters for each test case to ensure the dynamic process spawning works in most possible use cases. The parameters are the number of processes and applications we spawn in the test application. In the following, we refer to the number of processes as \say{\lstinline|np|} and the number of applications we spawn as \say{\lstinline|na|}.

The first test case contains a single run of \say{\lstinline|MPI_Comm_spawn|} in the test application, and we can vary the number of processes to spawn. The second test case runs the same spawn command twice sequentially. The third test case runs the \say{\lstinline|MPI_Comm_spawn_multiple|}, which can spawn multiple applications with varying numbers of processes. 

In each test case, the implementation validates the spawn request. The mock \gls{vrm} has control over this process, which helps a fully implemented \gls{vrm} to fulfil \glspl{sla} as mentioned in section \ref{Introduction}. If the mock \gls{vrm} does not approve the dynamic process spawning, the wrapper must forward the feedback to the \gls{mpi} process that initiated the request. We test this procedure by denying the dynamic spawning with the mock \gls{vrm} for every test case once.

The virtual test cluster we use for testing is limited to two compute nodes that can run two processes each. For this reason, we limit the number of processes we can dynamically spawn. In the following, we list every test case and the parameters we use for their execution.

\begin{description}
	\item[Test case 1 (np=1)]
		The first test case spawns a single application. This subcase considers the dynamic spawning of a single process of an application. For testing purposes, we spawn the application itself. We can use the \say{\lstinline|ps -A|} command to list all processes on the current computer. Listing \ref{a_t1_plus} shows a protocol of bash commands run during and after the execution of the test case. We can see that the \say{\lstinline|hello|} application, which is the testing application, is correctly spawned once from the initial test case. After execution, all application processes terminate correctly. Running \lstinline|squeue| shows the jobs are no longer in the queue and thus completed.

		We can notice that the \lstinline|squeue| command often still displays the job as running, even though the process is no longer alive. We can see such a situation in listing \ref{a_t1_neg}. The \lstinline|squeue| command shows that the testing application still runs on the node \say{\lstinline|c1|}, but there is no process for it on the node. The output of the process, for which \lstinline|squeue| lists the job, is also incomplete. We could not identify the cause for this behaviour within the scope of this thesis.

		If we execute this test case with a mock \gls{vrm} that denies any request, then the \gls{mpi} application terminates immediately with an unknown \gls{mpi} error. This behaviour is not optimal for applications that only try to spawn more processes for a potential speed-up. Such applications can set a custom error handler using \lstinline|MPI_Comm_set_errhandler| to catch the failure \cite{ompi_spawn_doc}.
		
	\item[Test case 1 (np=2)]
		Running the first test case again but dynamically spawning two processes of the testing application results in a different behaviour. The current implementation fails to run the application at all. We can identify the reason for this as the missing implementation of the fence server method, which is evident by the debug output when the fence method runs. The development process revolved around the test cases with a single dynamically spawned process described in section \ref{dev}. The fence operation that was previously present in the source code was left without modification, which resulted in waiting during the \say{\lstinline|PMIx_Connect|} call in line $390$ of \lstinline|/ompi/ompi/dpm/dpm.c|.\\ After reaching a timeout, the parent process crashes with a segmentation fault shown in listing \ref{a_t2}.

		This issue only sometimes occurs. When both spawned processes run on a single node, the fence method does not run. However, a different issue exists that prevents the execution.
		\begin{lstlisting}[captionpos=b, caption={Missing key in the key value store.}, label=missing_key,captionpos=b]
			[slurm.pmix.17.0 : 0]	Lookup for key: 1048051712.0:3859509122:nextcid:recv:0
			[slurm.pmix.17.0]	Recv from Lookup: 0
			[slurm.pmix.17.0 : 0]	Abort called
		\end{lstlisting}
		Listing \ref{missing_key} shows a snipped of the log that the wrapper writes to a debug file. The wrapper may only receive nothing from the key-value store when the key is missing. No process that participates publishes this key in this specific case. A failed lookup automatically results in the termination of the application that requested the value in the current implementation. The reason for this is that OpenMPI does not expect such a response. Notably, the parent application hangs in this scenario after it has run a \gls{bcx} data request.
		
	\item[Test case 2 (np=1)]
	    The dynamically spawned processes in this test case start and terminate properly without residual job entries in \lstinline|squeue| that we observed in test case 1. However, the parent process is not capable of terminating.
	    
\begin{lstlisting}[captionpos=b, caption={MPI error of the parent application.},label=t2_ompi_fail,captionpos=b]
-------------------------------------------------------------------
WARNING: Open MPI failed to TCP connect to a peer MPI process.  This
should not happen.

Your Open MPI job may now hang or fail.

Local host: c1
PID:        972
Message:    connect() to 255.19.0.8:1025 failed
Error:      Connection timed out (110)
-------------------------------------------------------------------
pml:ob1: mca_pml_ob1_rndv_completion: operation failed with code -12
\end{lstlisting}
	
		Listing \ref{t2_ompi_fail} displays the error message that OpenMPI writes to the error log of the parent process. Further investigation shows that OpenMPI hangs at line $1927$ of \lstinline|/ompi/ompi/dpm/dpm.c|, which is responsible for disconnecting all communicators. The reason for this behaviour is not known. The consequence of this error is that the parent process never terminates.
		
		If we execute this test case with a mock \gls{vrm} that denies any request, then the \gls{mpi} application terminates immediately and does not run the second spawn request.
		
	\item[Test case 2 (np=2)]
		Running the second test case but dynamically spawning two processes twice instead of one process yields a different outcome. The behaviour is similar to the first test case when spawning two processes simultaneously. Notably, after the parent process dynamically spawned the first two processes, it does not continue to spawn the last two. This behaviour occurs because the process hangs or fails at the first call of \say{\lstinline|MPI_Comm_spawn|}.
		
	\item[Test case 3 (na=3, np=1,1,1)]
		The third test case concerns spawning multiple applications with varying amounts of processes at once. If we run three applications with a single process, their behaviour is the same as in the first test case that spawns a single application with one process. The spawn method of the implementation runs the \lstinline|srun| command for every application. This procedure causes \gls{slurm} to consider each application a job, resulting in the previously existing PMIx functionality of \gls{slurm} to assign a different PMIx namespace to each application. This behaviour coincides with each application process's rank being $0$. 
		
		The MPI processes require communicators to communicate with each other. The \say{\lstinline|MPI_Comm_spawn_multiple|} method only fills a single communicator. Any MPI communication method requires the user to pass the destination, which could be ambiguous because every spawned process has a rank of 0. However, the rank must identify the destination uniquely. Thus, an issue may arise if the parent application wants to communicate with a specific spawned process using the communicator.
		
		If we run this test case with a mock \gls{vrm} that denies any spawn request, then the behaviour is the same as in the first test case. The application terminates immediately.
		
	\item[Test case 3 (na=2, np=2,1)]
		We reran the third test case but spawned two applications that had two and one process, respectively. In this configuration, the residual job entries in the output of \lstinline|squeue| appear. In contrast to the first test case that spawned an application with two processes, the application with two processes properly terminates in some cases. However, all dynamically spawned processes may encounter the issue of a key missing in the key-value store. We could not find the root cause for this behaviour, which appears random.
		
		Specifically, the issue occurs inside of the \say{\lstinline|ompi_comm_nextcid|} method call at line $540$ of \lstinline|/ompi/ompi/dpm/dpm.c|. The \say{\lstinline|send_first|} variable should ensure that one side first transmits a key-value pair for the other to look up. However, neither side of the key-value exchange sends the key-value pair first.
		
	\item[Test case 3 (na=2, np=2,2)]
		If we rerun the third test case with an elevated number of application processes to exceed the total resources available in the virtual cluster, then we run into allocation problems. Each compute node of the virtual cluster only has two CPU cores assigned. \gls{slurm} tries to allocate a CPU core to each spawned process. We have two compute nodes, which allow for four processes at maximum. The spawn request exceeds this limit because the parent process also reserves resources.
\begin{lstlisting}[captionpos=b, label=alloc, caption={\lstinline|squeue| output when resources cannot be allocated.}]
root@c1:/home# squeue
JOBID PARTITION  NAME    USER ST       TIME  NODES NODELIST(REASON)
7    normal    hello     root PD       0:00      1 (Resources)
5    normal MpiTestS     root  R       0:02      1 c1
6    normal    hello     root  R       0:01      1 c2
\end{lstlisting}
		Listing \ref{alloc} shows the allocation problem that the implementation faces when the test application spawns too many dynamic processes. The parent process tries to wait for all applications to spawn, but one of the applications has to wait for \gls{slurm} to allocate resources to it. This results in a potentially long wait time for the application, which blocks the resources in the cluster while waiting.
\end{description}