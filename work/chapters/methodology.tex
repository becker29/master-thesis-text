\section{Methodology}
\label{Methodology}

The dynamic process spawning in the context of a plan-based \gls{slurm} can be a complex proof of concept implementation. We must establish the various requirements for our implementation. Multiple approaches to the development process exist as well. Both are important to the methodology with which we work on the solution.

\subsection{Functional Requirements}
\label{req}

We have two fundamental functional requirements for features the proof of concept must have.

\begin{description}
	\item[Allow/Deny process spawning (F1)] The implementation must contact the \gls{vrm} before allowing the dynamic process spawning to enable an intervention. The implementation must then represent this state to OpenMPI. When the \gls{vrm} allows the process spawning, then the spawning process must continue. When the \gls{vrm} denies the process spawning, the implementation must provide a status indicating a failure to the request's initiator. We will evaluate this functionality using the mocked \gls{vrm} of the extension described in section \ref{Current State}. We consider the requirement fulfilled if the \gls{vrm} has control over the execution of all dynamic spawn requests issued during the execution of the test cases.
	
	\item[Enable dynamic process spawning (F2)] The proof of concept should be able to show that it is possible to dynamically spawn processes in \gls{slurm} with PMIx and OpenMPI. Process spawning must work with the two MPI methods we can use to spawn dynamic processes. Section \ref{subsec:OMPI} discussed both methods. We can verify this requirement by running a test case for each method of spawning dynamic processes. We vary the amount of processes to spawn to ensure that the functionalities work for varying amounts of processes. We assume the implementation enables dynamic process spawning if the test cases run successfully. We declare a run to be successful if all application processes terminate without human intervention after all spawned processes print their debug messages. Additionally, all dynamic processes must initialize without human intervention.
\end{description}

\subsection{Testing}

We need to test the proof of concept after the development. The software project that established the extension described in section \ref{Current State} uses a virtual cluster. We need to use the virtual cluster for testing because no functional \gls{slurm} cluster is available to us.

With a suitable test program, we must test requirements \textbf{F1} and \textbf{F2}. We have already met the two MPI methods that we can use to spawn dynamic processes in section \ref{subsec:OMPI}. The primary testing program of the software project is at \lstinline|/ompi/rank-swapper-agent/hello_c.c| and uses these methods. The testing program has three modes. The first mode spawns another process using \say{\lstinline|MPI_Comm_spawn|}. The second mode spawns a process twice using the same method, and the last mode spawns multiple processes at once using \say{\lstinline|MPI_Comm_spawn_multiple|}. To limit the scope of this work, it should suffice if the testing program works. We will check each mode with varying process counts passed to the spawn methods. These test cases cover the acceptance criteria of \textbf{F2}. We can run the same test cases with a mock \gls{vrm} configured to deny any spawn request to validate requirement \textbf{F1}.

The testing program will be submitted with \lstinline|sbatch| using the job script below. It runs the \say{\lstinline|hello|} \gls{mpi} test application once. The log files have unique names because they contain the \gls{slurm} job identifier.\\

\begin{BVerbatim}
#!/bin/bash
#SBATCH -N 1                     # Use a single node
#SBATCH -n 1                     # Run a single task
#SBATCH --ntasks-per-node 2      # Run two tasks per node
#SBATCH --output=log-%x.%j.out   # Where to write the output
#SBATCH --error=log-%x.%j.err    # Where to write error messages
#SBATCH --plan=PLAN              # Job validator plan

# Run the testing program
srun --mpi=pmix /home/ompi/rank-swapper-agent/hello
\end{BVerbatim}

\subsection{Development}
\label{dev}

In section \ref{Current State}, we surveyed the current state of participating applications and systems. We have already discovered the incomplete PMIx support of \gls>{slurm} for dynamic process spawning in section \ref{state_slurm}. It is straightforward that the implementation of missing functionality in the PMIx plugin of \gls{slurm} is required. However, there are multiple approaches to implement that functionality:

\begin{enumerate}
	\item Implement all of the functionality within \gls{slurm}, which may include the implementation of multiple data structures to hold required information.
	\item Include some functionality within \gls{slurm} but keep the data structures within the extension.
	\item Keep functionality and data structures in the extension and only implement wrappers in \gls{slurm}.
\end{enumerate}

A significant flaw of the first approach is the missing adaptability to other \gls{rms}. The current extension is nearly independent of the \gls{rms}. The only interfaces that exist between the \gls{rms} and the extension are the job validator and DPM agent, as can be seen in sections \ref{ext_arch} and \ref{Job Validator}. The DPM agent does not need \gls{slurm} because it is only a \gls{spank} plugin to start and stop itself. Another issue is the maintenance of the functionality. Newer versions of \gls{slurm} may include fundamental changes. 

Implementing data structures in C is a challenge as well. An example of this could be a data structure that holds the key-value pairs described in the publish, lookup and unpublish server methods in section \ref{pmix_methods}. We could implement the data structure as a list. However, a hashtable would be more performant. Such a hashtable exists within the standard C++ libraries and is accessible through the \say{\lstinline|std::unordered_map|}. Memory management is less prone to errors with modern C++ because smart pointers automatically release resources when they are no longer in use. Still, a significant benefit is the existing environment that \gls{slurm} provides. Like the \say{\lstinline|common|} module of the extension, \gls{slurm} also has common functionalities that we can use.

The second approach shares most of the flaws with the first. Even though we can use the data structures in C++, the approach introduces additional overhead because the C implementation will have to communicate with the extension to access these data structures.

The third approach shares the overhead of the second approach. Additionally, we would have to introduce more communication. However, this approach requires the most communication because all the PMIx requests must be wrapped and implemented in the extension. Benefits of the approach include the adaptability to other \gls{rms}, more straightforward implementation of data structures and fewer maintenance issues due to the logic's independence. For these reasons, the third approach is the most preferable.

Similar to the location of the implementation, there are also multiple ways in which the development itself can take place:

\begin{enumerate}
	\item Work closely related to a use case and only implement what we need for the next step.
	\item Plan most of the implementation ahead according to the PMIx specification of the server methods described in section \ref{pmix_methods}.
\end{enumerate}

The development strategies differ in their adaptability to changes. Because the thesis goal is a proof of concept, we might not require a complete implementation of every method according to the PMIx specification. For this reason, we cannot plan most of the implementation. All methods we require for the proof of concept may only become apparent after the development has begun, so we prefer the first development method.